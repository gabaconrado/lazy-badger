# Lazy Badger

> A simple utility to run scripts in the project level to boost your workflow

## Overview

Lazy-badger let you call scripts in a pre-configured directory in your machine
to make your workflows more smooth in a consistent way across different projects.

It works by running any shell script inside a `scripts` folder in the run directory.

## Usage

Create a `scripts` folder in any project and add some shell scripts to it:

```bash
cd my-project
mkdir scripts
# The ".sh" extension is important
echo "echo 'hello'" > scripts/hello.sh
# Works for nested directories too!
mkdir scripts/nested
# And it is possible to take arguments
echo "echo 'hello $@'" > scripts/nested/hello-nested.sh

chmod u+x scripts/hello.sh
chmod u+x scripts/nested/hello.sh
```

Then simply call then by their name:

```bash
cd my-project
lazy-badger hello
$ hello
lazy-badger hello-nested "world"
$ hello world
```

## It is basically make with files instead of commands, then?

Basically, the differences are:

- Instead of commands inside a `Makefile`, the name of the scripts identify them;
- You can pass arguments normally to the scripts;
- You can't specify dependencies between the scripts (yet);

The project was motivated by `make`, specifically by making it simpler since I
always used `make` more like a script centralizer than as a building tool anyways.

## Install

To install it, just go to the [releases][1] page, download your chosen version
and put it in a directory covered by your `PATH`.

[1]: https://gitlab.com/gabaconrado/lazy-badger/-/releases
