#!/usr/bin/env bash

# This script runs linting in the repository
#
# Requires the shellcheck binary

shellcheck ./src/*.sh
