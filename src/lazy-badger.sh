#!/usr/bin/env bash

# TODO: Implement help and list commands

_scripts_path="scripts"

if [ ! -d "${_scripts_path}" ]; then
    echo "Scripts directory not found"
    exit 1
fi

cmd=${1}

if [ -z "${cmd}" ]; then
    echo "No command passed"
    exit 2
fi

shift

set -e

_script="$(find ${_scripts_path} -type f -name "${cmd}.sh")"
_script_count="$(echo "${_script}" | wc -l)"
if [ -z "${_script}" ]; then
    echo "No script found: ${cmd}"
    exit 3
fi
if [[ "${_script_count}" -gt 1 ]]; then
    echo -e "Multiple scripts found with the same name:\n${_script}"
    exit 4
fi

${_script} "${*}"

set +e
